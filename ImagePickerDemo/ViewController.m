//
//  ViewController.m
//  ImagePickerDemo
//
//  Created by James Cash on 14-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickImage:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
//    picker.mediaTypes = @[kUTTypeMovie];
    NSLog(@"Picking from %ld media types %@", picker.sourceType, picker.mediaTypes);

    picker.delegate = self;

    [self presentViewController:picker animated:YES completion:^{
        NSLog(@"Presented");
    }];
}

#pragma mark - UIIMagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSLog(@"Picked %@", info);
    UIImage *pickedImage = info[@"UIImagePickerControllerOriginalImage"];
    self.imageView.image = pickedImage;
    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{

}

@end
